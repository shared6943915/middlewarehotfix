 # Middleware hotfix application
 The project represents the application, which is useful when creating the Middleware hotfixes.

 This application gives you a chance to do the common necessary parts of creating hotfix.

 The application is JavaFX application.
 In the window, you can set the version of hotfix.
 Once you set the version, the application shows you the steps necessary for creating the hotfix.

 The application automatically does the following:
 - it extends the "version-descriptor.yml" files in all modules with the correct extension string
 - it changes the versions in the "pom.xml" files in all modules.
 - it copies the downloaded WAR files to the Wildfly->standalone->deployments folder

 ## How to use
 To use the application, you need to download the build of the application.

 The build contains the necessary JDK files, the built application, and the batch file for running the application.

 You have to have the Java JDK 17. This is not part of the build. You can download the JDK here: https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.zip

 To run the application using the batch file, you have to set the environment variable "JAVA_MW_HOTFIX_HOME" to point to the Java JDK 17.