﻿# Changelog

## [1.1.0] - 2024-04-28
**Features:**
- Rename and copy WAR files to deployments folder 
- Implement logging

**Refactors:**
- Refactor logging + add console logging
## [1.0.0] - 2024-04-21
**Features:**
- feat: Add error handling 
- feat: Change versions in pom.xml files 
- feat: Extend version-descriptor files 
- feat: Copy branch name and commit message to clipboard 
- feat: Show steps for creating hotfix 
- feat: Add internationalization 
- feat: Create main screen
