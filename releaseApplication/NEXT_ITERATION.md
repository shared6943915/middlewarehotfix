# Create next iteration
1. Checkout and update the DEVELOP branch.
2. Run the script next_iteration.ps1. This script will ask for the next version, change the version in the pom.xml files, and create and checkout the branch with name "next-iteration"
3. Check the changes and commit with the message "chore: next iteration". **DO NOT PUSH!!!**
4. Merge the MASTER branch and resolve conflicts. **FORCE PUSH**
5. Create the merge request to DEVELOP branch, and merge the MR.