# Create the release
1. Run the script "next_release.bat". This script will checkout the DEVELOP branch, update the branch, ask for the new version, change the version in pom.xml file, extend the CHANGELOG.md file, create and checkout the branch "release/XXX"
2. Check if the CHANGELOG.md file is complete
3. Run maven command "mvn clean validate compile test package" and copy "target/MiddlewareHotfix.jar" and "/target/lib" to "applicationBuild"
4. Commit with the message "chore: release XXX". **DO NOT PUSH!!!**
5. Merge the MASTER branch and resolve conflicts. **FORCE PUSH**
6. Create the merge request to the MASTER branch. **DO NOT SQUASH COMMITS**
7. Merge the merge request
8. Create release and tag in Git