@echo off
git checkout develop
git pull origin develop
set /p version=Specify the next version:
powershell .\extend_changelog.ps1 -nextVersion %version%
powershell .\create_release.ps1 -nextVersion %version%
