module com.madera.michal.middlewarehotfix {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;
    requires org.apache.commons.lang3;


    opens com.madera.michal.middlewarehotfix to javafx.fxml;
    opens com.madera.michal.middlewarehotfix.error;
    opens com.madera.michal.middlewarehotfix.mainscreen;
    opens com.madera.michal.middlewarehotfix.createhotfix;
    opens com.madera.michal.middlewarehotfix.logger;

    exports com.madera.michal.middlewarehotfix;
    exports com.madera.michal.middlewarehotfix.mainscreen;
    exports com.madera.michal.middlewarehotfix.internationalization;
    exports com.madera.michal.middlewarehotfix.createhotfix;
    exports com.madera.michal.middlewarehotfix.error;
    exports com.madera.michal.middlewarehotfix.mainscreen.exceptions;
    exports com.madera.michal.middlewarehotfix.logger;
}