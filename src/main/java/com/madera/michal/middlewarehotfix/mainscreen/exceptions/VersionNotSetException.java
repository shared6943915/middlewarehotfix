package com.madera.michal.middlewarehotfix.mainscreen.exceptions;

/**
 * Exception thrown when the version is not set.
 */
public class VersionNotSetException extends Exception {
    public VersionNotSetException() {
        super("Version is not set");
    }
}
