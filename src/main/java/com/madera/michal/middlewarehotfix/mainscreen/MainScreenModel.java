package com.madera.michal.middlewarehotfix.mainscreen;

import com.madera.michal.middlewarehotfix.createhotfix.ChangePomXmlVersionWorker;
import com.madera.michal.middlewarehotfix.createhotfix.CreateHotfixAction;
import com.madera.michal.middlewarehotfix.createhotfix.CreateHotfixStep;
import com.madera.michal.middlewarehotfix.createhotfix.ExtendVersionDescriptorWorker;
import com.madera.michal.middlewarehotfix.createhotfix.RenameWarFileWorker;
import com.madera.michal.middlewarehotfix.internationalization.InternationalizationModel;
import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.DeploymentsPathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.MiddlewarePathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.VersionNotSetException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The MainScreenModel class represents the model component of the main screen in an application.
 * It encapsulates the data and behavior of the main screen.
 */
public class MainScreenModel {
    private static final ILogger LOG = LoggerFactory.createLogger(MainScreenModel.class);

    private static final Integer NUMBER_OF_HOTFIX_STEPS = 13;

    private final InternationalizationModel internationalizationModel;
    private final List<CreateHotfixStep> createHotfixSteps;
    private String hotfixVersion;
    private String middlewarePath;
    private String wildflyDeploymentsPath;
    private final ExtendVersionDescriptorWorker extendVersionDescriptorWorker;
    private final ChangePomXmlVersionWorker changePomXmlVersionWorker;
    private final RenameWarFileWorker renameWarFileWorker;

    public MainScreenModel() {
        this.internationalizationModel = InternationalizationModel.getInstance();
        this.createHotfixSteps = new ArrayList<>(NUMBER_OF_HOTFIX_STEPS);
        initializeHotfixSteps();
        this.extendVersionDescriptorWorker = new ExtendVersionDescriptorWorker();
        this.changePomXmlVersionWorker = new ChangePomXmlVersionWorker();
        this.renameWarFileWorker = new RenameWarFileWorker();
    }

    /**
     * JUST FOR TESTS!!
     */
    MainScreenModel(
            final ExtendVersionDescriptorWorker extendVersionDescriptorWorker,
            final ChangePomXmlVersionWorker changePomXmlVersionWorker,
            final RenameWarFileWorker renameWarFileWorker
    ) {
        this.internationalizationModel = InternationalizationModel.getInstance();
        this.createHotfixSteps = new ArrayList<>(NUMBER_OF_HOTFIX_STEPS);
        initializeHotfixSteps();
        this.extendVersionDescriptorWorker = extendVersionDescriptorWorker;
        this.changePomXmlVersionWorker = changePomXmlVersionWorker;
        this.renameWarFileWorker = renameWarFileWorker;
    }

    private void initializeHotfixSteps() {
        LOG.info("Initialize hotfix steps");
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.checkout-base-branch", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.create-branch", CreateHotfixAction.COPY_TO_CLIPBOARD));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.cherry-pick-commit", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.extend-version-descriptors", CreateHotfixAction.EXTEND_VERSION_DESCRIPTOR));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.change-pom-xml-version", CreateHotfixAction.CHANGE_POM_XML));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.check-git-changes", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.commit-changes", CreateHotfixAction.COPY_TO_CLIPBOARD));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.wait-for-jenkins", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.download-build", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.extract-war-files", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.get-wf-deployments-path", CreateHotfixAction.SELECT_FOLDER));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.deploy-and-test", CreateHotfixAction.RENAME_WAR_FILES));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.copy-to-update-path", CreateHotfixAction.NONE));
        createHotfixSteps.add(new CreateHotfixStep("hotfix-step.create-tag", CreateHotfixAction.NONE));
        LOG.info("Hotfix steps initialized");
    }



    public String getTranslatedHotfixStep(final CreateHotfixStep step) {
        if (step.getStepTextKeyParameter() == null) {
            return internationalizationModel.getTranslatedString(step.getStepTextKey());
        }

        return internationalizationModel.getParametrizedTranslatedString(step.getStepTextKey(), step.getStepTextKeyParameter());
    }

    public List<CreateHotfixStep> getCreateHotfixSteps() {
        return createHotfixSteps;
    }

    public void setHotfixVersionNumber(final String hotfixVersionNumber) {
        LOG.info("Set hotfix version number to {}", hotfixVersionNumber);
        this.hotfixVersion = hotfixVersionNumber;
        createHotfixSteps.get(1).setStepTextKeyParameter(hotfixVersionNumber);
        createHotfixSteps.get(1).setActionParameter("hotfix/v" + hotfixVersionNumber);
        createHotfixSteps.get(6).setStepTextKeyParameter(hotfixVersionNumber);
        createHotfixSteps.get(6).setActionParameter("chore: release " + hotfixVersionNumber);
        LOG.info("Hotfix version number set");
    }

    public void setMiddlewarePath(final String middlewarePath) {
        this.middlewarePath = middlewarePath;
    }

    public void extendVersionDescriptor() throws IOException, MiddlewarePathNotSetException, VersionNotSetException {
        LOG.info("Extend version descriptors");
        if (middlewarePath == null) {
            LOG.error("Middleware path is missing");

            throw new MiddlewarePathNotSetException();
        }

        if (hotfixVersion == null) {
            LOG.error("Hotfix version is missing");

            throw new VersionNotSetException();
        }

        extendVersionDescriptorWorker.extendVersionDescriptorFiles(hotfixVersion, middlewarePath);
        LOG.info("Version descriptors extended");
    }

    public void changePomXmlVersions()
            throws XPathExpressionException, ParserConfigurationException, IOException, TransformerException, SAXException,
            MiddlewarePathNotSetException, VersionNotSetException {
        LOG.info("Change versions in POM.xml files");
        if (middlewarePath == null) {
            LOG.error("Middleware path is missing");

            throw new MiddlewarePathNotSetException();
        }

        if (hotfixVersion == null) {
            LOG.error("Version number is missing");

            throw new VersionNotSetException();
        }

        changePomXmlVersionWorker.changePomXmlVersions(middlewarePath, hotfixVersion);
        LOG.info("Versions in POM.xml files changed");
    }

    public void setWildflyDeploymentsPath(final String deploymentsPath) {
        this.wildflyDeploymentsPath = deploymentsPath;
    }

    public void renameWarFiles(final String archivesPath) throws IOException, DeploymentsPathNotSetException {
        LOG.info("Rename war files");

        if (wildflyDeploymentsPath == null) {
            LOG.error("Wildfly deployments path is missing");

            throw new DeploymentsPathNotSetException();
        }

        renameWarFileWorker.renameAndCopyWarFiles(archivesPath, wildflyDeploymentsPath);

        LOG.info("War files renamed");
    }
}
