package com.madera.michal.middlewarehotfix.mainscreen.exceptions;

/**
 * Exception thrown when the WF deployments path is not set.
 */
public class DeploymentsPathNotSetException extends Exception {
    public DeploymentsPathNotSetException() {
        super("Wildfly deployments path not set");
    }
}
