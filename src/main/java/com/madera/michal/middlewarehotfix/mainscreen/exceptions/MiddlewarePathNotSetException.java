package com.madera.michal.middlewarehotfix.mainscreen.exceptions;

/**
 * Exception thrown when the middleware path is not set.
 */
public class MiddlewarePathNotSetException extends Exception {
    public MiddlewarePathNotSetException() {
        super("Middleware path not set");
    }
}
