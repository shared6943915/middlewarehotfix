package com.madera.michal.middlewarehotfix.mainscreen;

import com.madera.michal.middlewarehotfix.createhotfix.CreateHotfixStep;
import com.madera.michal.middlewarehotfix.error.ErrorController;
import com.madera.michal.middlewarehotfix.internationalization.AvailableLanguages;
import com.madera.michal.middlewarehotfix.internationalization.InternationalizationModel;
import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.DeploymentsPathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.MiddlewarePathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.VersionNotSetException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * The MainScreenController class is responsible for controlling the main screen of the application.
 */
public class MainScreenController {
    private static final ILogger LOG = LoggerFactory.createLogger(MainScreenController.class);

    private static final String USER_HOME_DIR = System.getProperty("user.home");

    private static final String UNKNOWN_ERROR_HEADER = "error.unknown.header";

    private static final String UNKNOWN_ERROR_TEXT = "error.unknown.text";

    @FXML
    private Menu languageMenu;

    @FXML
    private TitledPane hotfixStepsTitlePane;

    @FXML
    private AnchorPane hotfixStepsPane;

    @FXML
    private TextField txtHotfixVersion;

    @FXML
    private Button showStepsButton;

    private List<CheckBox> hotfixStepCheckBoxes;

    private MainScreenModel model;
    private InternationalizationModel internationalizationModel;

    public void initialize(final MainScreenModel model) {
        this.model = model;
        this.internationalizationModel = InternationalizationModel.getInstance();
        this.hotfixStepCheckBoxes = new LinkedList<>();

        this.setComponentTexts(true);
        this.createLanguageMenuItems();
        this.initializeCheckboxes();

        txtHotfixVersion.textProperty().addListener((observable, oldValue, newValue) -> showStepsButton.setDisable(newValue == null || newValue.isEmpty()));
    }

    private void setComponentTexts(final boolean initial) {
        if (!initial) {
            ((Stage) hotfixStepsTitlePane.getScene().getWindow()).setTitle(internationalizationModel.getTranslatedString("main-screen.title"));
        }
        languageMenu.setText(internationalizationModel.getTranslatedString("menu-bar.language"));
        txtHotfixVersion.setPromptText(internationalizationModel.getTranslatedString("hotfix.version"));
        hotfixStepsTitlePane.setText(internationalizationModel.getTranslatedString("hotfix-step.header"));
        showStepsButton.setText(internationalizationModel.getTranslatedString("show-steps-button"));

        if (!hotfixStepsPane.getChildren().isEmpty()) {
            for (CheckBox checkBox : hotfixStepCheckBoxes) {
                checkBox.setText(model.getTranslatedHotfixStep((CreateHotfixStep) checkBox.getUserData()));
            }
        }
    }

    private void createLanguageMenuItems() {
        for (AvailableLanguages language : AvailableLanguages.values()) {
            CheckMenuItem menuItem = new CheckMenuItem(language.getLanguageName());
            menuItem.setSelected(language == internationalizationModel.getUsedLanguage());
            menuItem.setOnAction(event -> {
                languageMenu.getItems().forEach(item -> ((CheckMenuItem) item).setSelected(false));
                menuItem.setSelected(true);
                internationalizationModel.switchLanguage(language);
                this.setComponentTexts(false);
            });
            languageMenu.getItems().add(menuItem);
        }
    }

    private void initializeCheckboxes() {
        int y = 10;
        List<CreateHotfixStep> hotfixSteps = model.getCreateHotfixSteps();
        for (CreateHotfixStep hotfixStep : hotfixSteps) {
            CheckBox checkBox = new CheckBox(model.getTranslatedHotfixStep(hotfixStep));
            checkBox.setLayoutY(y);
            checkBox.setUserData(hotfixStep);
            checkBox.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
                if (Boolean.TRUE.equals(newValue)) {
                    doHotfixAction(hotfixStep, checkBox);
                }
            });
            hotfixStepCheckBoxes.add(checkBox);
            y += 25;
        }
    }

    private void doHotfixAction(final CreateHotfixStep hotfixStep, final CheckBox checkBox) {
        switch (hotfixStep.getAction()) {
            case COPY_TO_CLIPBOARD:
                LOG.info("Copy {} to clipboard", hotfixStep.getActionParameter());
                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();
                content.putString(hotfixStep.getActionParameter());
                clipboard.setContent(content);
                LOG.info("{} copied to clipboard", hotfixStep.getActionParameter());
                break;
            case EXTEND_VERSION_DESCRIPTOR:
                extendVersionDescriptors(checkBox);
                break;
            case CHANGE_POM_XML:
                changePomXmlVersions(checkBox);
                break;
            case RENAME_WAR_FILES:
                renameWarFiles(checkBox);
                break;
            case SELECT_FOLDER:
                selectFolder(checkBox);
                break;
            case NONE:
                // NONE action.
                break;
        }
    }

    private void selectFolder(final CheckBox checkBox) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        directoryChooser.setTitle(internationalizationModel.getTranslatedString("choose-wildfly-deployments-directory"));

        File chosenDirectory = directoryChooser.showDialog(hotfixStepsPane.getScene().getWindow());

        if (chosenDirectory != null) {
            model.setWildflyDeploymentsPath(chosenDirectory.getAbsolutePath());
        } else {
            checkBox.setSelected(false);
        }
    }

    private void extendVersionDescriptors(final CheckBox checkBox) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(USER_HOME_DIR));
        directoryChooser.setTitle(internationalizationModel.getTranslatedString("choose-middleware-directory"));

        File chosenDirectory = directoryChooser.showDialog(hotfixStepsPane.getScene().getWindow());

        if (chosenDirectory != null) {
            try {
                model.setMiddlewarePath(chosenDirectory.getAbsolutePath());
                model.extendVersionDescriptor();
            } catch (MiddlewarePathNotSetException e) {
                ErrorController.openErrorModal("error.middleware-path-not-set.header", "error.middleware-path-not-set.text");
                checkBox.setSelected(false);
            } catch (VersionNotSetException e) {
                ErrorController.openErrorModal("error.version-not-set.header", "error.version-not-set.text");
                checkBox.setSelected(false);
            } catch (IOException e) {
                LOG.error("Error occurred when extending version descriptors: {}", ExceptionUtils.getStackTrace(e));
                ErrorController.openErrorModal(UNKNOWN_ERROR_HEADER, UNKNOWN_ERROR_TEXT, ExceptionUtils.getStackTrace(e));
                checkBox.setSelected(false);
            }
        } else {
            checkBox.setSelected(false);
        }
    }

    private void changePomXmlVersions(final CheckBox checkBox) {
        try {
            model.changePomXmlVersions();
        } catch (MiddlewarePathNotSetException e) {
            ErrorController.openErrorModal("error.middleware-path-not-set.header", "error.middleware-path-not-set.text");
            checkBox.setSelected(false);
        } catch (VersionNotSetException e) {
            ErrorController.openErrorModal("error.version-not-set.header", "error.version-not-set.text");
            checkBox.setSelected(false);
        } catch (XPathExpressionException | ParserConfigurationException | IOException | TransformerException | SAXException e) {
            LOG.error("Error occurred when changing versions in pom.xml files: {}", ExceptionUtils.getStackTrace(e));
            ErrorController.openErrorModal(UNKNOWN_ERROR_HEADER, UNKNOWN_ERROR_TEXT, ExceptionUtils.getStackTrace(e));
            checkBox.setSelected(false);
        }
    }

    private void renameWarFiles(final CheckBox checkBox) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File(USER_HOME_DIR));
        directoryChooser.setTitle(internationalizationModel.getTranslatedString("choose-archives-directory"));

        File chosenDirectory = directoryChooser.showDialog(hotfixStepsPane.getScene().getWindow());

        if (chosenDirectory != null) {
            try {
                model.renameWarFiles(chosenDirectory.getAbsolutePath());
            } catch (DeploymentsPathNotSetException e) {
                ErrorController.openErrorModal("error.deployments-path-not-set.header", "error.deployments-path-not-set.text");
                checkBox.setSelected(false);
            } catch (IOException e) {
                LOG.error("Error occurred when renaming WAR files: {}", ExceptionUtils.getStackTrace(e));
                ErrorController.openErrorModal(UNKNOWN_ERROR_HEADER, UNKNOWN_ERROR_TEXT, ExceptionUtils.getStackTrace(e));
                checkBox.setSelected(false);
            }
        } else {
            checkBox.setSelected(false);
        }
    }

    @FXML
    private void showHotfixSteps() {
        model.setHotfixVersionNumber(txtHotfixVersion.getText());
        for (CheckBox checkBox : hotfixStepCheckBoxes) {
            checkBox.setText(model.getTranslatedHotfixStep((CreateHotfixStep) checkBox.getUserData()));
        }
        hotfixStepsPane.getChildren().addAll(hotfixStepCheckBoxes);
    }
}
