package com.madera.michal.middlewarehotfix.logger;

/**
 * This interface defines the contract for a logger which is used for logging information, warnings, and errors.
 * It provides methods for logging messages at different severity levels and managing log levels.
 */
public interface ILogger {
    void info(String message, Object... args);

    void warn(String message, Object... args);

    void error(String message, Object... args);
}
