package com.madera.michal.middlewarehotfix.logger;

/**
 * The ConsoleLogger class is an implementation of the ILogger interface that logs messages to the console.
 * It extends the LoggerMessageCreator class to generate log messages.
 */
public class ConsoleLogger extends LoggerMessageCreator implements ILogger {
    ConsoleLogger(final String className) {
        super(className);
    }

    @Override
    public void info(String message, Object... args) {
        writeMessage(LogType.INFO, message, args);
    }

    @Override
    public void warn(String message, Object... args) {
        writeMessage(LogType.WARN, message, args);
    }

    @Override
    public void error(String message, Object... args) {
        writeMessage(LogType.ERROR, message, args);
    }

    private void writeMessage(final LogType type, final String message, final Object... messageArguments) {
        String logMessage = createLogMessage(type, message, messageArguments);

        if (type == LogType.ERROR) {
            System.err.println(logMessage);
        } else {
            System.out.println(logMessage);
        }
    }
}
