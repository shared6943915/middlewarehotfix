package com.madera.michal.middlewarehotfix.logger;

/**
 * The LoggerFactory class is responsible for creating and managing instances of Logger.
 * It provides a centralized way of obtaining logger instances for different parts of the application.
 * Logger instances are used for logging messages and diagnostics information.
 */
public class LoggerFactory {
    private LoggerFactory() {}

    public static ILogger createLogger(final Class<?> clazz) {
        return new FileLogger(clazz.getName());
    }
}
