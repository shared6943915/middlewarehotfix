package com.madera.michal.middlewarehotfix.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The FileLogger class is responsible for logging messages to a file.
 * It extends the LoggerMessageCreator class and implements the ILogger interface.
 * The logs are saved to a file with a filename format of "server_YYYYMMDD.log",
 * where "YYYYMMDD" represents the current date.
 * The class provides methods for logging information messages, warning messages, and error messages.
 * The log messages are saved to the log file in the order they are received.
 */
public class FileLogger extends LoggerMessageCreator implements ILogger {
    private final String fileName;

    FileLogger(final String className) {
        super(className);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        this.fileName = "logs" + File.separator + "server_" + formatter.format(LocalDate.now()) + ".log";

        File folder = new File("logs");
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    /**
     * JUST FOR TESTS!!!
     */
    FileLogger(final String className, final String fileName) {
        super(className);
        this.fileName = fileName;
    }

    @Override
    public void info(final String message, final Object... args) {
        saveLogMessage(LogType.INFO, message, args);
    }

    @Override
    public void warn(final String message, final Object... args) {
        saveLogMessage(LogType.WARN, message, args);
    }

    @Override
    public void error(final String message, final Object... args) {
        saveLogMessage(LogType.ERROR, message, args);
    }

    private void saveLogMessage(final LogType type, final String message, final Object... messageArguments) {
        try {
            String logMessage = createLogMessage(type, message, messageArguments);

            FileWriter fileWriter = new FileWriter(fileName, true);
            PrintWriter writer = new PrintWriter(fileWriter);
            writer.println(logMessage);
            writer.close();
        } catch (IOException e) {
            // Should not happen...
        }
    }
}
