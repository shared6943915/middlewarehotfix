package com.madera.michal.middlewarehotfix.error;

import com.madera.michal.middlewarehotfix.internationalization.InternationalizationModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

public class ErrorController {
    @FXML
    private Label errorTitle;

    @FXML
    private TextArea errorText;

    @FXML
    private Button okButton;

    public static void openErrorModal(final String errorTitleKey, final String errorTextKey) {
        openErrorModal(errorTitleKey, errorTextKey, null);
    }

    public static void openErrorModal(final String errorTitleKey, final String errorTextKey, final String errorTextParameter) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ErrorController.class.getResource("/fxml/error-screen.fxml"));
            Scene scene = new Scene(loader.load());
            ErrorController controller = loader.getController();
            controller.initialize(errorTitleKey, errorTextKey, errorTextParameter);
            Stage stage = new Stage();
            stage.setAlwaysOnTop(true);
            stage.setTitle("ERROR");
            stage.setResizable(false);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            // Should not happen...
        }
    }

    public void initialize(final String errorTitleKey, final String errorTextKey, final String errorTextParameter) {
        InternationalizationModel internationalization = InternationalizationModel.getInstance();

        errorTitle.setText(internationalization.getTranslatedString(errorTitleKey));
        if (errorTextParameter != null) {
            errorText.setText(internationalization.getParametrizedTranslatedString(errorTextKey, errorTextParameter));
        } else {
            errorText.setText(internationalization.getTranslatedString(errorTextKey));
        }
        okButton.setText(internationalization.getTranslatedString("buttons.ok"));
    }

    @FXML
    private void closeModal() {
        ((Stage) okButton.getScene().getWindow()).close();
    }
}
