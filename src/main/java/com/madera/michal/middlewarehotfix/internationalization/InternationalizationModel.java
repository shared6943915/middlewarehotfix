package com.madera.michal.middlewarehotfix.internationalization;


import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class InternationalizationModel {
    private static final ILogger LOG = LoggerFactory.createLogger(InternationalizationModel.class);

    private static final String MESSAGES_BUNDLE_NAME = "com.madera.michal.middlewarehotfix.messages";
    private static final AvailableLanguages DEFAULT_LANGUAGE = AvailableLanguages.SLOVAK;

    private ResourceBundle bundle;
    private Locale locale;
    private AvailableLanguages usedLanguage;

    private static InternationalizationModel instance = null;

    private InternationalizationModel() {
        this.locale = new Locale(DEFAULT_LANGUAGE.getLanguageCode(), DEFAULT_LANGUAGE.getCountryCode());
        this.bundle = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME, locale);
        this.usedLanguage = DEFAULT_LANGUAGE;
    }

    public static InternationalizationModel getInstance() {
        if (instance == null) {
            instance = new InternationalizationModel();
        }

        return instance;
    }

    public String getTranslatedString(final String key) {
        return bundle.getString(key);
    }

    public String getParametrizedTranslatedString(final String key, final String parameter) {
        return MessageFormat.format(getTranslatedString(key), parameter);
    }

    public void switchLanguage(final AvailableLanguages language) {
        LOG.info("Switch to language {}", language);
        this.locale = new Locale(language.getLanguageCode(), language.getCountryCode());
        this.bundle = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME, locale);
        this.usedLanguage = language;
        LOG.info("Language changed to {}", language);
    }

    public AvailableLanguages getUsedLanguage() {
        return usedLanguage;
    }
}
