package com.madera.michal.middlewarehotfix.internationalization;

/**
 * Represents the available languages for a software application.
 */
public enum AvailableLanguages {
    SLOVAK("sk", "SK", "Slovenčina"),
    ENGLISH("en", "US", "English");

    private final LanguageDTO language;

    AvailableLanguages(final String languageCode, final String countryCode, final String languageName) {
        this.language = new LanguageDTO(languageCode, countryCode, languageName);
    }

    public String getLanguageCode() {
        return language.getLanguageCode();
    }

    public String getCountryCode() {
        return language.getCountryCode();
    }

    public String getLanguageName() {
        return language.getLanguageName();
    }
}
