package com.madera.michal.middlewarehotfix.internationalization;

/**
 * Represents a language data transfer object.
 * This class encapsulates the data of a language and provides methods to access and modify the data.
 */
public class LanguageDTO {
    private final String languageCode;

    private final String countryCode;

    private final String languageName;

    public LanguageDTO(final String languageCode, final String countryCode, final String languageName) {
        this.languageCode = languageCode;
        this.countryCode = countryCode;
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getLanguageName() {
        return languageName;
    }
}
