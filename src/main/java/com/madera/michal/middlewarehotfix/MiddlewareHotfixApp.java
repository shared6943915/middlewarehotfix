package com.madera.michal.middlewarehotfix;

import com.madera.michal.middlewarehotfix.internationalization.InternationalizationModel;
import com.madera.michal.middlewarehotfix.mainscreen.MainScreenController;
import com.madera.michal.middlewarehotfix.mainscreen.MainScreenModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The MiddlewareHotfixApp class is the main class of the JavaFX application.
 * It launches the application and controls the main screen.
 */
public class MiddlewareHotfixApp extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        MainScreenModel model = new MainScreenModel();
        InternationalizationModel internationalizationModel = InternationalizationModel.getInstance();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MiddlewareHotfixApp.class.getResource("/fxml/main-screen.fxml"));
        Scene scene = new Scene(loader.load());
        MainScreenController controller = loader.getController();
        controller.initialize(model);
        stage.setTitle(internationalizationModel.getTranslatedString("main-screen.title"));
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
