package com.madera.michal.middlewarehotfix.createhotfix;

import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * This class represents a worker that extends the version descriptor of a system.
 */
public class ExtendVersionDescriptorWorker {
    private static final ILogger LOG = LoggerFactory.createLogger(ExtendVersionDescriptorWorker.class);

    private static final String VERSION_DESCRIPTOR_RELATIVE_PATH = File.separator + "src" + File.separator
            + "main" + File.separator + "resources" + File.separator + File.separator + "version-descriptor.yml";

    private final ModulePath modulePathGetter;

    public ExtendVersionDescriptorWorker() {
        this.modulePathGetter = ModulePath.getInstance();
    }

    /**
     * JUST FOR TESTS!!!
     */
    ExtendVersionDescriptorWorker(final ModulePath modulePathGetter) {
        this.modulePathGetter = modulePathGetter;
    }

    public void extendVersionDescriptorFiles(final String version, final String middlewarePath) throws IOException {
        LOG.info("Extend version-descriptor files in the middleware path {} with version {}", middlewarePath, version);
        List<String> versionDescriptorPaths = getVersionDescriptorPaths(middlewarePath);

        for (String versionDescriptorPath : versionDescriptorPaths) {
            extendVersionDescriptor(version, versionDescriptorPath);
        }
        LOG.info("version-descriptor files extended.");
    }

    private List<String> getVersionDescriptorPaths(final String middlewarePath) throws IOException {
        return modulePathGetter.getModulePaths(middlewarePath, true)
                .stream()
                .map(modulePath -> modulePath + VERSION_DESCRIPTOR_RELATIVE_PATH)
                .toList();
    }

    private void extendVersionDescriptor(final String version, final String filePath) throws IOException {
        LOG.info("Extend version-descriptor file at path {}", filePath);
        Path versionDescriptorPath = Paths.get(filePath);
        String existingContent = Files.readString(versionDescriptorPath);
        String newContent = existingContent + createExtentionString(version);
        Files.writeString(versionDescriptorPath, newContent);
        LOG.info("version-descriptor file at path {} extended.", filePath);
    }

    private String createExtentionString(final String version) {
        StringBuilder result = new StringBuilder();
        result.append("- version: \"").append(version).append("\"").append("\n");
        result.append("  components:").append("\n");
        result.append("  - name: ").append("\"shared\"").append("\n");
        result.append("    componentVersion: \"").append(version).append("\"").append("\n");
        result.append("  - name: ").append("\"common\"").append("\n");
        result.append("    componentVersion: \"").append("1.1.0").append("\"").append("\n");

        return result.toString();
    }
}
