package com.madera.michal.middlewarehotfix.createhotfix;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ModulePath {
    private static final String SHARED_FOLDER_NAME = "shared";

    private static final List<String> EXCLUDED_FOLDER_NAMES = Arrays.asList(
           SHARED_FOLDER_NAME, "middleware", ".git", ".githooks", ".idea", "mysql", "src", "target"
    );

    private static ModulePath instance = null;

    private ModulePath() {}

    public static ModulePath getInstance() {
        if (instance == null) {
            instance = new ModulePath();
        }

        return instance;
    }

    public List<String> getModulePaths(final String middlewarePath, final boolean excludeShared) throws IOException {
        List<String> usedExcludedFolderNames;
        if (excludeShared) {
            usedExcludedFolderNames = EXCLUDED_FOLDER_NAMES;
        } else {
            usedExcludedFolderNames = EXCLUDED_FOLDER_NAMES.stream().filter(folderName -> !folderName.equals(SHARED_FOLDER_NAME)).toList();
        }

        try (Stream<Path> paths = Files.walk(Paths.get(middlewarePath), 1)) {
            return paths
                    .filter(Files::isDirectory)
                    .filter(folder -> !usedExcludedFolderNames.contains(folder.getFileName().toString()))
                    .map(folder -> folder.toAbsolutePath().toString())
                    .toList();
        }
    }
}
