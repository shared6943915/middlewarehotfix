package com.madera.michal.middlewarehotfix.createhotfix;

/**
 * This enum represents the different actions that can be performed for creating a hotfix.
 */
public enum CreateHotfixAction {
    NONE,
    EXTEND_VERSION_DESCRIPTOR,
    CHANGE_POM_XML,
    COPY_TO_CLIPBOARD,
    RENAME_WAR_FILES,
    SELECT_FOLDER;

    public boolean isNone() {
        return equals(NONE);
    }

    public boolean isExtendVersionDescriptor() {
        return equals(EXTEND_VERSION_DESCRIPTOR);
    }

    public boolean isChangePomXml() {
        return equals(CHANGE_POM_XML);
    }

    public boolean isCopyToClipboard() {
        return equals(COPY_TO_CLIPBOARD);
    }

    public boolean isRenameWarFiles() {
        return equals(RENAME_WAR_FILES);
    }

    public boolean isSelectFolder() {
        return equals(SELECT_FOLDER);
    }
}
