package com.madera.michal.middlewarehotfix.createhotfix;

/**
 * The CreateHotfixStep class represents a step in a hotfix creation process.
 * It provides methods to perform various actions related to creating a hotfix.
 */
public class CreateHotfixStep {
    private final String stepTextKey;

    private String stepTextKeyParameter;

    private final CreateHotfixAction action;

    private String actionParameter;

    public CreateHotfixStep(final String stepTextKey, final CreateHotfixAction action) {
        this.stepTextKey = stepTextKey;
        this.action = action;
    }

    public String getStepTextKey() {
        return stepTextKey;
    }

    public String getStepTextKeyParameter() {
        return stepTextKeyParameter;
    }

    public void setStepTextKeyParameter(final String stepTextKeyParameter) {
        this.stepTextKeyParameter = stepTextKeyParameter;
    }

    public CreateHotfixAction getAction() {
        return action;
    }

    public String getActionParameter() {
        return actionParameter;
    }

    public void setActionParameter(final String actionParameter) {
        this.actionParameter = actionParameter;
    }
}
