package com.madera.michal.middlewarehotfix.createhotfix;

import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * The ChangePomXmlVersionWorker class is responsible for modifying the version attribute in a pom.xml file.
 */
public class ChangePomXmlVersionWorker {
    private static final ILogger LOG = LoggerFactory.createLogger(ChangePomXmlVersionWorker.class);

    private static final String POM_XML_RELATIVE_PATH = File.separator + "pom.xml";

    private final ModulePath modulePathGetter;

    public ChangePomXmlVersionWorker() {
        this.modulePathGetter = ModulePath.getInstance();
    }

    /**
     * JUST FOR TESTS!!!
     */
    ChangePomXmlVersionWorker(final ModulePath modulePathGetter) {
        this.modulePathGetter = modulePathGetter;
    }

    public void changePomXmlVersions(final String middlewarePath, final String version)
            throws XPathExpressionException, ParserConfigurationException, IOException, TransformerException, SAXException {
        LOG.info("Change POM.xml versions in middleware path {} to version {}", middlewarePath, version);
        List<File> pomXmlFiles = getPomXmlFiles(middlewarePath);

        for (File pomXml : pomXmlFiles) {
            modifyPomXmlVersion(pomXml, version, false);
        }
        File mainPomXml = new File(middlewarePath + POM_XML_RELATIVE_PATH);
        modifyPomXmlVersion(mainPomXml, version, true);
        LOG.info("POM.xml versions changed");
    }

    private List<File> getPomXmlFiles(final String middlewarePath) throws IOException {
        List<String> pomXmlPaths = modulePathGetter.getModulePaths(middlewarePath, false)
                .stream()
                .map(modulePath -> modulePath + POM_XML_RELATIVE_PATH)
                .toList();

        return pomXmlPaths.stream()
                .map(File::new)
                .toList();
    }

    private void modifyPomXmlVersion(final File pomXmlFile, final String newVersion, final boolean mainPomXmlFile)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        LOG.info("Changing the version in POM.xml {} to version {}. Main pom.XML?: {}", pomXmlFile.getAbsolutePath(), newVersion, mainPomXmlFile);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(pomXmlFile);
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr;

        if (mainPomXmlFile) {
            expr = xpath.compile("/project/version");
        } else {
            expr = xpath.compile("/project/parent/version");
        }

        Node versionNode = (Node) expr.evaluate(document, XPathConstants.NODE);
        if (versionNode != null) {
            LOG.info("Change current version {} to version {}", versionNode.getTextContent(), newVersion);
            versionNode.setTextContent(newVersion);
        } else {
            LOG.error("No <version> node in the pom.xml file {}", pomXmlFile.getAbsolutePath());
            throw new IllegalStateException("Version node not found.");
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(pomXmlFile);
        transformer.transform(source, result);
        LOG.info("Version in POM.xml {} changed.", pomXmlFile.getAbsolutePath());
    }
}
