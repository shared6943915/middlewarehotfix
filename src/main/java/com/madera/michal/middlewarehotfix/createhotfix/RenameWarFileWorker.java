package com.madera.michal.middlewarehotfix.createhotfix;

import com.madera.michal.middlewarehotfix.logger.ILogger;
import com.madera.michal.middlewarehotfix.logger.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * The RenameWarFileWorker class is responsible for renaming and copying WAR files from an archive to a specified directory.
 * It uses the ILogger interface for logging information, warnings, and errors.
 */
public class RenameWarFileWorker {
    private static final ILogger LOG = LoggerFactory.createLogger(RenameWarFileWorker.class);

    private static final List<String> EXCLUDED_FOLDER_NAMES = Arrays.asList(
            "shared", "parent", "bbp"
    );

    public void renameAndCopyWarFiles(final String archivePath, final String wildflyPath) throws IOException {
        LOG.info("Rename and copy WAR files from archive path {} to wildfly path {}", archivePath, wildflyPath);
        List<String> warFileFolders = getFolders(archivePath);

        for (String warFileFolder : warFileFolders) {
            Path warFileFolderPath = Paths.get(warFileFolder);
            String finalWarFileName = warFileFolderPath.getFileName().toString() + ".war";

            File warFile = getWarFile(warFileFolderPath);

            LOG.info("Renaming and copying war file {} to {}", warFile.getName(), wildflyPath);
            Path destinationPath = Paths.get(wildflyPath, finalWarFileName);
            Files.copy(warFile.toPath(), destinationPath, StandardCopyOption.REPLACE_EXISTING);
        }

        LOG.info("WAR files renamed and copied");
    }

    private List<String> getFolders(final String archivePath) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(archivePath), 1)) {
            return paths
                    .filter(Files::isDirectory)
                    .filter(folder -> !EXCLUDED_FOLDER_NAMES.contains(folder.getFileName().toString()))
                    .map(folder -> folder.toAbsolutePath().toString())
                    .toList();
        }
    }

    private File getWarFile(final Path warFileFolderPath) throws IOException {
        try (Stream<Path> paths = Files.walk(warFileFolderPath)) {
            return paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".war"))
                    .map(Path::toFile)
                    .findFirst()
                    .orElseThrow(() -> new IOException("No war file found in " + warFileFolderPath.toAbsolutePath()));
        }
    }
}
