package com.madera.michal.middlewarehotfix.mainscreen;

import com.madera.michal.middlewarehotfix.createhotfix.ChangePomXmlVersionWorker;
import com.madera.michal.middlewarehotfix.createhotfix.CreateHotfixStep;
import com.madera.michal.middlewarehotfix.createhotfix.ExtendVersionDescriptorWorker;
import com.madera.michal.middlewarehotfix.createhotfix.RenameWarFileWorker;
import com.madera.michal.middlewarehotfix.internationalization.AvailableLanguages;
import com.madera.michal.middlewarehotfix.internationalization.InternationalizationModel;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.DeploymentsPathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.MiddlewarePathNotSetException;
import com.madera.michal.middlewarehotfix.mainscreen.exceptions.VersionNotSetException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the {@link MainScreenModel}
 */
class MainScreenModelTest {
    private ExtendVersionDescriptorWorker extendVersionDescriptorWorker;
    private ChangePomXmlVersionWorker changePomXmlVersionWorker;
    private RenameWarFileWorker renameWarFileWorker;

    private MainScreenModel model;

    private final InternationalizationModel internationalizationModel = InternationalizationModel.getInstance();

    @BeforeEach
    public void setUp() {
        extendVersionDescriptorWorker = Mockito.mock(ExtendVersionDescriptorWorker.class);
        changePomXmlVersionWorker = Mockito.mock(ChangePomXmlVersionWorker.class);
        renameWarFileWorker = Mockito.mock(RenameWarFileWorker.class);

        model = new MainScreenModel(extendVersionDescriptorWorker, changePomXmlVersionWorker, renameWarFileWorker);
        internationalizationModel.switchLanguage(AvailableLanguages.SLOVAK);
    }

    @Test
    void initializeHotfixSteps() {
        List<CreateHotfixStep> hotfixSteps = model.getCreateHotfixSteps();

        assertEquals(14, hotfixSteps.size());
    }

    @Test
    void getTranslatedHotfixStep() {
        model.setHotfixVersionNumber("1.0.0");
        assertEquals("Cherry-pick commit s fixom", model.getTranslatedHotfixStep(model.getCreateHotfixSteps().get(2)));

        assertEquals("Vytvorit branch s nazvom \"hotfix/v1.0.0\". Po zaskrtnuti tohto checkboxu bude nazov branch skopirovany do clipboard.", model.getTranslatedHotfixStep(model.getCreateHotfixSteps().get(1)));
    }

    @Test
    void setHotfixVersionNumber() {
        model.setHotfixVersionNumber("1.0.0");

        assertEquals("1.0.0", model.getCreateHotfixSteps().get(1).getStepTextKeyParameter());
        assertEquals("1.0.0", model.getCreateHotfixSteps().get(6).getStepTextKeyParameter());
        assertEquals("hotfix/v1.0.0", model.getCreateHotfixSteps().get(1).getActionParameter());
        assertEquals("chore: release 1.0.0", model.getCreateHotfixSteps().get(6).getActionParameter());
        assertEquals("Vytvorit branch s nazvom \"hotfix/v1.0.0\". Po zaskrtnuti tohto checkboxu bude nazov branch skopirovany do clipboard.", model.getTranslatedHotfixStep(model.getCreateHotfixSteps().get(1)));
        assertEquals("Commit so spravou \"chore: release 1.0.0\", a pushnut. Po zaskrtnuti tohto checkboxu bude commit message skopirovana do clipboard.", model.getTranslatedHotfixStep(model.getCreateHotfixSteps().get(6)));
    }

    @Test
    void extendVersionDescriptor() throws Exception {
        model.setHotfixVersionNumber("1.0.0");
        model.setMiddlewarePath("mw-path");

        model.extendVersionDescriptor();

        Mockito.verify(extendVersionDescriptorWorker).extendVersionDescriptorFiles("1.0.0", "mw-path");
        Mockito.verifyNoMoreInteractions(extendVersionDescriptorWorker);
        Mockito.verifyNoInteractions(changePomXmlVersionWorker);
    }

    @Test
    void extendVersionDescriptorShouldThrowExceptionWhenMiddlewarePathNotSet() {
        model.setHotfixVersionNumber("1.0.0");

        Assertions.assertThrows(MiddlewarePathNotSetException.class, () -> model.extendVersionDescriptor(), "Middleware path not set");

        Mockito.verifyNoInteractions(extendVersionDescriptorWorker, changePomXmlVersionWorker);
    }

    @Test
    void extendVersionDescriptorShouldThrowExceptionWhenHotfixNumberNotSet() {
        model.setMiddlewarePath("mw-path");

        Assertions.assertThrows(VersionNotSetException.class, () -> model.extendVersionDescriptor(), "Version number not set");

        Mockito.verifyNoInteractions(extendVersionDescriptorWorker, changePomXmlVersionWorker);
    }

    @Test
    void changePomXmlVersion() throws Exception {
        model.setHotfixVersionNumber("1.0.0");
        model.setMiddlewarePath("mw-path");

        model.changePomXmlVersions();

        Mockito.verify(changePomXmlVersionWorker).changePomXmlVersions("mw-path", "1.0.0");
        Mockito.verifyNoMoreInteractions(changePomXmlVersionWorker);
        Mockito.verifyNoInteractions(extendVersionDescriptorWorker);
    }

    @Test
    void changePomXmlVersionShouldThrowExceptionWhenMiddlewarePathNotSet() {
        Assertions.assertThrows(MiddlewarePathNotSetException.class, () -> model.changePomXmlVersions(), "Middleware path not set");

        Mockito.verifyNoInteractions(changePomXmlVersionWorker, extendVersionDescriptorWorker);
    }

    @Test
    void changePomXmlVersionShouldThrowExceptionWhenVersionNotSet() {
        model.setMiddlewarePath("mw-path");

        Assertions.assertThrows(VersionNotSetException.class, () -> model.changePomXmlVersions(), "Version number not set");

        Mockito.verifyNoInteractions(changePomXmlVersionWorker, extendVersionDescriptorWorker);
    }

    @Test
    void renameWarFiles() throws Exception {
        model.setWildflyDeploymentsPath("wildfly/deployments");

        model.renameWarFiles("archives/f1/f2/bbp");

        Mockito.verify(renameWarFileWorker).renameAndCopyWarFiles("archives/f1/f2/bbp", "wildfly/deployments");
        Mockito.verifyNoMoreInteractions(renameWarFileWorker);
        Mockito.verifyNoInteractions(changePomXmlVersionWorker, extendVersionDescriptorWorker);
    }

    @Test
    void renameWarFilesShouldThrowExceptionWhenDeploymentsFolderNotSet() throws Exception {
        Assertions.assertThrows(DeploymentsPathNotSetException.class, () -> model.renameWarFiles("archives"), "Wildfly deployments path not set");

        Mockito.verifyNoInteractions(renameWarFileWorker, changePomXmlVersionWorker, extendVersionDescriptorWorker);
    }
}
