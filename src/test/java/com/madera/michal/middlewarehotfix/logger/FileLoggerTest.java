package com.madera.michal.middlewarehotfix.logger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for the {@link FileLogger}
 */
class FileLoggerTest {
    private String logFileName;
    private final String className = FileLoggerTest.class.getName();

    private FileLogger logger;

    @BeforeEach
    void setUp() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String folderPath = new File(Objects.requireNonNull(FileLoggerTest.class.getResource("/logs")).toURI()).getAbsolutePath();
        logFileName = folderPath + File.separator + "server_" + formatter.format(LocalDate.now()) + ".log";

        File logFile = new File(logFileName);
        if (logFile.exists()) {
            Files.delete(logFile.toPath());
        } else {
            Files.createFile(logFile.toPath());
        }

        logger = new FileLogger(className, logFileName);
    }

    @Test
    void logInfoWithoutArguments() throws Exception {
        String message = "This is test message w/o arguments";

        logger.info(message);

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.INFO.getLogPrefix(), message);
    }

    @Test
    void logInfoWithArguments() throws Exception {
        String message = "This is test message with argument: {}";
        String expectedMessage = "This is test message with argument: ARG1";

        logger.info(message, "ARG1");

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.INFO.getLogPrefix(), expectedMessage);
    }

    @Test
    void warnInfoWithoutArguments() throws Exception {
        String message = "This is warning test message w/o arguments";

        logger.warn(message);

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.WARN.getLogPrefix(), message);
    }

    @Test
    void warnInfoWithArguments() throws Exception {
        String message = "This is warning message with argument: {}";
        String expectedMessage = "This is warning message with argument: ARG1";

        logger.warn(message, "ARG1");

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.WARN.getLogPrefix(), expectedMessage);
    }

    @Test
    void errorInfoWithoutArguments() throws Exception {
        String message = "This is error message w/o arguments";

        logger.error(message);

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.ERROR.getLogPrefix(), message);
    }

    @Test
    void errorInfoWithArguments() throws Exception {
        String message = "This is error message with argument: {}";
        String expectedMessage = "This is error message with argument: ARG1";

        logger.error(message, "ARG1");

        List<String> lines = logFileLines();
        assertEquals(1, lines.size());
        assertLogFileLine(lines.get(0), LogType.ERROR.getLogPrefix(), expectedMessage);
    }

    @Test
    void shouldAppendMessagesCorrectly() throws Exception {
        String message1 = "This is first message w/o arguments";
        String message2 = "This is the second message. It has two arguments: {}, {}";
        String message3 = "This is error message";
        String message4 = "This is another error message with argument {}";
        String expectedMessage2 = "This is the second message. It has two arguments: Argument 1, Second longer argument";
        String expectedMessage4 = "This is another error message with argument ARG1";

        logger.info(message1);
        logger.warn(message2, "Argument 1", "Second longer argument");
        logger.error(message3);
        logger.error(message4, "ARG1");

        List<String> lines = logFileLines();
        assertEquals(4, lines.size());
        assertLogFileLine(lines.get(0), LogType.INFO.getLogPrefix(), message1);
        assertLogFileLine(lines.get(1), LogType.WARN.getLogPrefix(), expectedMessage2);
        assertLogFileLine(lines.get(2), LogType.ERROR.getLogPrefix(), message3);
        assertLogFileLine(lines.get(3), LogType.ERROR.getLogPrefix(), expectedMessage4);
    }

    private List<String> logFileLines() throws Exception {
        Path logFile = Paths.get(logFileName);

        return Files.readAllLines(logFile);
    }

    private void assertLogFileLine(final String line, final String expectedLogType, final String expectedMessage) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String expectedDate = formatter.format(LocalDate.now());
        String dateTimePatternString = expectedDate + " \\d{2}:\\d{2}:\\d{2}";
        Pattern pattern = Pattern.compile(dateTimePatternString);
        String dateTimeSubstring = line.substring(0, 19);
        String logTypeSubstring = line.substring(20, 25);
        String classSubstring = line.substring(26, 98);
        String messageSubstring = line.substring(99);
        boolean matches = pattern.matcher(dateTimeSubstring).matches();

        assertTrue(matches, "The datetime does not match the expected format");
        assertEquals(expectedLogType, logTypeSubstring.replaceAll("\\s", ""));
        assertEquals("[" + className + "]", classSubstring.replaceAll("\\s", ""));
        assertEquals(expectedMessage, messageSubstring.trim());
    }
}
