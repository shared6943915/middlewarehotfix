package com.madera.michal.middlewarehotfix.createhotfix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Test class for the {@link RenameWarFileWorker}
 */
class RenameWarFileWorkerTest {
    private RenameWarFileWorker worker;

    private String archivesPath;

    private String deploymentsPath;

    private List<String> deployedWarFilesPaths;

    @BeforeEach
    void setUp() throws Exception {
        File archives = new File(Objects.requireNonNull(RenameWarFileWorkerTest.class.getResource("/archives/f1/f2/bbp")).toURI());
        File deployments = new File(Objects.requireNonNull(RenameWarFileWorkerTest.class.getResource("/wildfly/standalone/deployments")).toURI());

        archivesPath = archives.getAbsolutePath();
        deploymentsPath = deployments.getAbsolutePath();

        deployedWarFilesPaths = Arrays.asList(
                deploymentsPath + File.separator + "war-service-1.war",
                deploymentsPath + File.separator + "war-service-2.war",
                deploymentsPath + File.separator + "war-service-3.war"
        );

        worker = new RenameWarFileWorker();
    }

    @Test
    void renameAndCopyWarFiles() throws Exception {
        worker.renameAndCopyWarFiles(archivesPath, deploymentsPath);

        List<String> renamedWarFilesPaths = deployedWarFilesPaths.stream()
                .toList();

        for (String path : renamedWarFilesPaths) {
            System.out.println("Checking: " + path);
            File warFile = new File(path);
            Assertions.assertTrue(warFile.exists());
        }
    }
}
