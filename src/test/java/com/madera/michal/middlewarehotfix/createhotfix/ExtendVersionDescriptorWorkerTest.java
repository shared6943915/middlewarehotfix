package com.madera.michal.middlewarehotfix.createhotfix;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Test class for the {@link ExtendVersionDescriptorWorker}
 */
class ExtendVersionDescriptorWorkerTest {
    private ExtendVersionDescriptorWorker worker;

    private String testProjectPath;

    private List<String> modulePaths;

    @BeforeEach
    void setUp() throws Exception {
        File f = new File(Objects.requireNonNull(ExtendVersionDescriptorWorkerTest.class.getResource("/middleware")).toURI());
        testProjectPath = f.getAbsolutePath();
        modulePaths = Arrays.asList(
                f.getAbsolutePath() + File.separator + "module1",
                f.getAbsolutePath() + File.separator + "module2",
                f.getAbsolutePath() + File.separator + "module3"
        );
        ModulePath modulePathGetter = Mockito.mock(ModulePath.class);
        Mockito.when(modulePathGetter.getModulePaths(anyString(), anyBoolean())).thenReturn(modulePaths);

        worker = new ExtendVersionDescriptorWorker(modulePathGetter);
    }

    @Test
    void extendVersionDescriptorFiles() throws Exception {
        worker.extendVersionDescriptorFiles("7.2.1", testProjectPath);

        for (String modulePath: modulePaths) {
            File versionDescriptorFile = getVersionDescriptorFile(modulePath);

            assertChangedVersionDescriptor(versionDescriptorFile, "7.2.1");
        }
    }

    private File getVersionDescriptorFile(final String modulePath) throws Exception {
        return new File(modulePath + File.separator + "src" + File.separator
                + "main" + File.separator + "resources" + File.separator + File.separator + "version-descriptor.yml");
    }

    private void assertChangedVersionDescriptor(final File versionDescriptorFile, final String expectedVersion) throws Exception {
        List<String> addedLines = getLast7Lines(versionDescriptorFile);

        assertEquals(7, addedLines.size());
        assertEquals("- version: \"" + expectedVersion + "\"", addedLines.get(0));
        assertEquals("  components:", addedLines.get(1));
        assertEquals("  - name: \"shared\"", addedLines.get(2));
        assertEquals("    componentVersion: \"" + expectedVersion + "\"", addedLines.get(3));
        assertEquals("  - name: \"common\"", addedLines.get(4));
        assertEquals("    componentVersion: \"1.1.0\"", addedLines.get(5));
    }

    private List<String> getLast7Lines(final File versionDescriptorFile) throws Exception {
        List<String> result = new ArrayList<>(7);

        try (RandomAccessFile raf = new RandomAccessFile(versionDescriptorFile, "r")) {
            long lastLinePosition = raf.length();
            raf.seek(lastLinePosition);

            int lines = 0;
            while (lastLinePosition > 0) {
                lastLinePosition--;
                raf.seek(lastLinePosition);
                if (raf.readByte() == '\n') {
                    lines++;
                    if (lines == 7) {
                        break;
                    }
                }
            }

            byte[] bytes = new byte[(int) (raf.length() - lastLinePosition)];
            raf.read(bytes);

            result.addAll(List.of(new String(bytes).split("\n")));
        }

        return result;
    }
}
