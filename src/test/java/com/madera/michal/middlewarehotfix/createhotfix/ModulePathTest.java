package com.madera.michal.middlewarehotfix.createhotfix;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for the {@link ModulePath}
 */
class ModulePathTest {
    private String testProjectPath;

    private ModulePath modulePathGetter;

    @BeforeEach
    void setUp() throws Exception {
        File f = new File(Objects.requireNonNull(ChangePomXmlVersionNumberTest.class.getResource("/middleware")).toURI());
        testProjectPath = f.getAbsolutePath();

        modulePathGetter = ModulePath.getInstance();
    }

    @Test
    void getModulePathsWithSharedModule() throws Exception {
        List<String> expectedModulePaths = Arrays.asList(
                testProjectPath + File.separator + "module1",
                testProjectPath + File.separator + "module2",
                testProjectPath + File.separator + "module3",
                testProjectPath + File.separator + "shared"
        );

        List<String> result = modulePathGetter.getModulePaths(testProjectPath, false);

        assertEquals(expectedModulePaths.size(), result.size());
        for (String expectedModulePath : expectedModulePaths) {
            assertTrue(result.contains(expectedModulePath));
        }
    }

    @Test
    void getModulePathsWithoutSharedModule() throws Exception {
        List<String> expectedModulePaths = Arrays.asList(
                testProjectPath + File.separator + "module1",
                testProjectPath + File.separator + "module2",
                testProjectPath + File.separator + "module3"
        );

        List<String> result = modulePathGetter.getModulePaths(testProjectPath, true);

        assertEquals(expectedModulePaths.size(), result.size());
        for (String expectedModulePath : expectedModulePaths) {
            assertTrue(result.contains(expectedModulePath));
        }
    }
}
