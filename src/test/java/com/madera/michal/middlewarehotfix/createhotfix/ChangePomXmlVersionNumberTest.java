package com.madera.michal.middlewarehotfix.createhotfix;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * Test class for the {@link ChangePomXmlVersionWorker}
 */
class ChangePomXmlVersionNumberTest {
    private ChangePomXmlVersionWorker worker;

    private String testProjectPath;

    private List<String> modulePaths;

    @BeforeEach
    void setUp() throws Exception {
        File f = new File(Objects.requireNonNull(ChangePomXmlVersionNumberTest.class.getResource("/middleware")).toURI());
        testProjectPath = f.getAbsolutePath();
        modulePaths = Arrays.asList(
                f.getAbsolutePath() + File.separator + "module1",
                f.getAbsolutePath() + File.separator + "module2",
                f.getAbsolutePath() + File.separator + "module3",
                f.getAbsolutePath() + File.separator + "shared"
        );
        ModulePath modulePathGetter = Mockito.mock(ModulePath.class);
        Mockito.when(modulePathGetter.getModulePaths(anyString(), anyBoolean())).thenReturn(modulePaths);

        worker = new ChangePomXmlVersionWorker(modulePathGetter);
    }

    @Test
    void changePomXmlVersions() throws Exception {
        worker.changePomXmlVersions(testProjectPath, "1.0.0");

        for (String modulePath : modulePaths) {
            File pomXmlFile = getPomXmlFile(modulePath);
            assertChangedVersion(pomXmlFile, "1.0.0", false);
        }

        File mainPomXmlFile = getPomXmlFile(testProjectPath);
        assertChangedVersion(mainPomXmlFile, "1.0.0", true);
    }

    private File getPomXmlFile(final String modulePath) throws Exception {
        return new File(modulePath + File.separator + "pom.xml");
    }

    private void assertChangedVersion(final File pomXmlFile, final String expectedVersion, final boolean mainPomXmlFile) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(pomXmlFile);
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr;

        if (mainPomXmlFile) {
            expr = xpath.compile("/project/version");
        } else {
            expr = xpath.compile("/project/parent/version");
        }
        Node versionNode = (Node) expr.evaluate(document, XPathConstants.NODE);

        if (versionNode != null) {
            assertEquals(expectedVersion, versionNode.getTextContent());
        } else {
            fail("Version node not found in pom.xml");
        }
    }
}
